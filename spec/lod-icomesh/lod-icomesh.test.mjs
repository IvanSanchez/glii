import Glii from "../../src/index.mjs";
import { default as icomesh } from "../../node_modules/icomesh/index.js";
// import { mat2 } from "../../3rd-party/gl-matrix.mjs";

const minLoD = 0;
const maxLoD = 5;

describe("LoD Icosphere", function () {
	it("renders normal facets", function () {
		const context = contextFactory(256, 256, { preserveDrawingBuffer: true });

		const glii = new Glii(context);

		const coordAttrib = new glii.SingleAttribute({
			glslType: "vec3",
			size: 10,
			growFactor: 1.2,
		});
		const normalAttrib = new glii.SingleAttribute({
			glslType: "vec3",
			size: 256,
			growFactor: 2,
		});
		const edgeAttrib = new glii.SingleAttribute({
			glslType: "vec3",
			size: 15,
			growFactor: 1.3,
		});

		let indices = new glii.LoDIndices({
			size: 4,
			growFactor: 1.2,
			type: glii.UNSIGNED_INT,
		});

		const edgeData = [
			[1, 0, 0],
			[0, 1, 0],
			[0, 0, 1],
		];

		let attribCount = 0;

		// 		for (let lod=minLoD; lod<=maxLoD; lod++) {
		for (let lod = maxLoD; lod >= minLoD; lod--) {
			const { vertices: coords, triangles: trigvertices } = icomesh(lod);

			const trigCount = trigvertices.length;

			for (let i = 0, l = trigvertices.length; i < l; i += 3) {
				const v1 = trigvertices[i + 0] * 3; // Index for the coords of vertex 1 in the coords array
				const v2 = trigvertices[i + 1] * 3; // Idem, vertex 2
				const v3 = trigvertices[i + 2] * 3; // Idem, vertex 3
				const c1 = coords.subarray(v1, v1 + 3); // Value of the XYZ coords for vertex 1
				const c2 = coords.subarray(v2, v2 + 3); // Idem, vertex 2
				const c3 = coords.subarray(v3, v3 + 3); // Idem, vertex 3
				// 			console.log("Face out of vertices: ", i,
				// 				trigvertices.subarray(i, i + 3),
				// 				v1, v2, v3,
				// 				c1, c2, c3
				// 			);
				const normal = [
					(c1[0] + c2[0] + c3[0]) / 3, // x
					(c1[1] + c2[1] + c3[1]) / 3, // y
					(c1[2] + c2[2] + c3[2]) / 3, // z
				];
				// Create a new triangle with indices [i, i+1, i+2], as well as
				// fill up the coords buffer for these new vertex indices
				// For icospheres with normals and edge data, vertices are **not**
				// reused, which simplifies assignments
				const vtx = indices.allocateSet(lod, [0, 2, 1], true);

				coordAttrib.set(vtx + 0, c1);
				coordAttrib.set(vtx + 1, c2);
				coordAttrib.set(vtx + 2, c3);
				normalAttrib.set(vtx + 0, normal);
				normalAttrib.set(vtx + 1, normal);
				normalAttrib.set(vtx + 2, normal);
				edgeAttrib.set(vtx + 0, edgeData[0]);
				edgeAttrib.set(vtx + 1, edgeData[1]);
				edgeAttrib.set(vtx + 2, edgeData[2]);
			}
		}

		let clear = new glii.WebGL1Clear({ color: [0.0, 0.0, 0.0, 1.0] });
		let program = new glii.WebGL1Program({
			vertexShaderSource: `

void main() {
// 	vNormal = normalize( ( (aNormal /2.) + vec3(.5) ) * lightSource );
	vNormal = vec4(normalize(aNormal), 1);
	gl_Position = vec4(aCoords.xyz, 1.0);
}`,
			varyings: { vNormal: "vec4" },
			fragmentShaderSource: `
vec4 lightSource1 = vec4(1., 1., -1., 1.);
vec4 lightSource2 = vec4(-.5, -.7, -1.2, 1.);

void main() {
	vec4 L = normalize(lightSource1 - vNormal);
	float NdotL = max (dot(vNormal,L), 0.);
	vec4 diffuse = NdotL * vec4(0.3, 0.1, 0.6, 1.0);

	gl_FragColor = vec4(0.1, 0.1, 0.1, 1.0) + diffuse;

	gl_FragColor +=  max(dot(vNormal,
							normalize(lightSource2 - vNormal)
						   ), 0.) * vec4(1.2, 0.2, 0.3, 1.0);
}`,
			indexBuffer: indices,
			attributes: {
				aCoords: coordAttrib,
				aNormal: normalAttrib,
			},
			depth: glii.LEQUAL,
		});

		let matches = [];
		function renderLoD(lod) {
			clear.run();
			program.run(lod);

			matches.push(
				expectPixelmatch(context, `spec/lod-icomesh/lod-icomesh-${lod}`, 15)
			);
		}

		for (let lod = minLoD; lod <= maxLoD; lod++) {
			renderLoD(lod);
		}

		let programEdge = new glii.WebGL1Program({
			vertexShaderSource: `
vec3 lightSource = vec3(1., 1., -1.);

void main() {
	vEdge = aEdge;
	vNormal = vec4(normalize(aNormal), 1);
// 	vNormal.xz = uRotMatrix * vNormal.xz;

	gl_Position = vec4(aCoords.xyz, 1.0);
// 	gl_Position.xz = uRotMatrix * aCoords.xz;
}`,
			varyings: { vNormal: "vec4", vEdge: "vec3" },
			fragmentShaderSource: `
vec4 lightSource1 = vec4(1., 1., -1., 1.);
vec4 lightSource2 = vec4(-.5, -.7, -1.2, 1.);
void main() {
	vec4 L = normalize(lightSource1 - vNormal);
	float NdotL = max (dot(vNormal,L), 0.);
	vec4 diffuse = NdotL * vec4(0.3, 0.1, 0.6, 1.0);

	gl_FragColor = vec4(0.1, 0.1, 0.1, 1.0) + diffuse;

	gl_FragColor +=  max(dot(vNormal,
							normalize(lightSource2 - vNormal)
						   ), 0.) * vec4(0.2, 1.2, 0.3, 1.0);

	gl_FragColor.gb += vec2(smoothstep(0.05, 0.01, min(min(vEdge.x, vEdge.y), vEdge.z)))
			* vec2(.5, .8);

// 	if (gl_FrontFacing) {
// 		gl_FragColor.r = 1.;
// 	} else {
// 		gl_FragColor.g += .1;
// 	}
}`,
			indexBuffer: indices,
			attributes: {
				aCoords: coordAttrib,
				aNormal: normalAttrib,
				aEdge: edgeAttrib,
			},
			// 			uniforms: { uRotMatrix: "mat2" },
			depth: glii.LEQUAL,
		});

		for (let lod = minLoD; lod <= 2; lod++) {
			clear.run();
			programEdge.run(lod);
			matches.push(
				expectPixelmatch(context, `spec/lod-icomesh/lod-edge-${lod}`, 300)
			);
		}

		indices.deallocateLoD(1);
		indices.deallocateLoD(2);
		indices.deallocateLoD(5);
		renderLoD(0);
		renderLoD(3);
		renderLoD(4);

		return Promise.all(matches);
	});
});
