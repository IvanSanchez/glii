import Glii from "../../src/index.mjs";

describe("Programmatic triangles with aCoords+aColor", function () {
	it("Renders using WireframeTriangleIndices", function () {
		const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
		const glii = new Glii(context);

		var vertexShaderSource = `
void main() {
	vColor = aColor;
	gl_Position = vec4(aCoords, 1.0);
}`;
		let varyings = { vColor: "vec4" };
		var fragmentShaderSource = `precision mediump float;
void main() {
	gl_FragColor = vColor;
}`;

		const coords = new glii.SingleAttribute({
			glslType: "vec3",
			size: 256,
			growFactor: false,
		});
		const color = new glii.SingleAttribute({
			glslType: "vec4",
			size: 256,
			growFactor: false,
		});

		let triangles = new glii.WireframeTriangleIndices({
			size: 256,
			growFactor: false,
			width: 1,
		});

		const delta = Math.PI / 30;
		for (let i = 0; i < 20; i++) {
			const angle = (Math.PI * i) / 10;
			const angle1 = i % 2 ? angle - delta : angle + delta;
			const angle2 = i % 2 ? angle + delta : angle - delta;
			const j = i * 3;

			const trig = new triangles.Triangle().setVertices(j, j + 1, j + 2);

			coords.set(j, [
				-0.1 + 0.1 * Math.cos(angle1),
				-0.1 + 0.1 * Math.sin(angle1),
				-0.5,
			]);
			color.set(j, [1.0, 0.0, 0.0, 1.0]);

			coords.set(j + 1, [
				0.1 + 0.8 * Math.cos(angle1),
				0.1 + 0.8 * Math.sin(angle1),
				-0.5,
			]);
			color.set(j + 1, [0.0, 1.0, 0.0, 1.0]);

			coords.set(j + 2, [
				0.1 + 0.8 * Math.cos(angle2),
				0.1 + 0.8 * Math.sin(angle2),
				-0.5,
			]);
			color.set(j + 2, [0.0, 0.0, 1.0, 1.0]);
		}

		let clear = new glii.WebGL1Clear({ color: [1.0, 1.0, 1.0, 1.0] });
		let program = new glii.WebGL1Program({
			vertexShaderSource: vertexShaderSource,
			varyings: varyings,
			fragmentShaderSource: fragmentShaderSource,
			indexBuffer: triangles,
			attributes: { aCoords: coords, aColor: color },
		});

		clear.run();
		program.run();

		return expectPixelmatch(context, "spec/programmatic-triangles/wireframe", 560);
	});
});
