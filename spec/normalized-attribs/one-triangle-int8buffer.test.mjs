import Glii from "../../src/index.mjs";

describe("One triangle, one normalized Int8 SingleAttribute", function () {
	it("renders red over clear green", function () {
		const context = contextFactory(256, 256, { preserveDrawingBuffer: true });

		const glii = new Glii(context);

		var vertexShaderSource = `
void main() {
	gl_Position = vec4(aCoords, 1.0);
}`;
		var fragmentShaderSource = `precision mediump float;
void main() {
  gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
}`;

		const coords = new glii.SingleAttribute({
			glslType: "vec3",
			type: Int8Array,
			normalized: true,
			size: 3,
			growFactor: false,
		});

		let indices = new glii.SequentialIndices({ size: 3 });

		coords.set(0, [-64, -64, -64]);
		coords.set(1, [-64, +64, +64]);
		coords.set(2, [+64, 0, 0]);

		let clear = new glii.WebGL1Clear({ color: [0.0, 1.0, 0.0, 1.0] });
		let program = new glii.WebGL1Program({
			vertexShaderSource: vertexShaderSource,
			varyings: {},
			fragmentShaderSource: fragmentShaderSource,
			indexBuffer: indices,
			attributes: { aCoords: coords },
		});

		clear.run();
		program.run();

		return expectPixelmatch(context, "spec/normalized-attribs/red-over-green", 1);
	});

	it("renders a triangle in a corner", function () {
		const context = contextFactory(256, 256, { preserveDrawingBuffer: true });

		const glii = new Glii(context);

		var vertexShaderSource = `
void main() {
	gl_Position = vec4(aCoords, 1.0);
}`;
		var fragmentShaderSource = `precision mediump float;
void main() {
  gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
}`;

		const coords = new glii.SingleAttribute({
			glslType: "vec3",
			type: Int8Array,
			normalized: true,
			size: 3,
			growFactor: false,
		});

		let indices = new glii.SequentialIndices({ size: 3 });

		coords.set(0, [-127, -127, 0]);
		coords.set(1, [-127, -112, 0]);
		coords.set(2, [-112, -127, 0]);

		let clear = new glii.WebGL1Clear({ color: [0.0, 0.0, 0.0, 0.0] });
		let program = new glii.WebGL1Program({
			vertexShaderSource: vertexShaderSource,
			varyings: [],
			fragmentShaderSource: fragmentShaderSource,
			indexBuffer: indices,
			attributes: { aCoords: coords },
		});

		clear.run();
		program.run();

		return expectPixelmatch(context, "spec/normalized-attribs/one-corner", 16);
	});
});

describe("One triangle, two Int8+Uint8 `SingleAttribute`s", function () {
	it("One triangle with color per vertex", function () {
		const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
		const glii = new Glii(context);

		var vertexShaderSource = `
varying vec4 vColor;

void main() {
	vColor = aColor;
	gl_Position = vec4(aCoords, 1.0);
}`;
		var fragmentShaderSource = `precision mediump float;
varying vec4 vColor;

void main() {
  gl_FragColor = vColor;
}`;

		const coords = new glii.SingleAttribute({
			glslType: "vec3",
			type: Int8Array,
			normalized: true,
			size: 3,
			growFactor: false,
		});
		const color = new glii.SingleAttribute({
			glslType: "vec4",
			type: Uint8Array,
			normalized: true,
			size: 3,
			growFactor: false,
		});

		let indices = new glii.SequentialIndices({ size: 3 });

		coords.set(0, [-100, -100, -64]);
		color.set(0, [255, 0, 0, 255]);

		coords.set(1, [-90, +80, +64]);
		color.set(1, [0, 255, 0, 255]);

		coords.set(2, [+100, -20, 0.0]);
		color.set(2, [0, 0, 255, 255]);

		let clear = new glii.WebGL1Clear({ color: [0.0, 0.0, 0.0, 0.0] });
		let program = new glii.WebGL1Program({
			vertexShaderSource: vertexShaderSource,
			varyings: [],
			fragmentShaderSource: fragmentShaderSource,
			indexBuffer: indices,
			attributes: { aCoords: coords, aColor: color },
		});

		clear.run();
		program.run();

		const queue = expectPixelmatch(
			context,
			"spec/normalized-attribs/coords-and-color",
			5
		);

		/// NOTE: A value of +128 turns into -1.0 when normalized.
		coords.set(0, [-127, -127, -64]);
		color.set(0, [128, 255, 0, 255]);

		coords.set(1, [-127, +127, +64]);
		color.set(1, [255, 0, 128, 255]);

		coords.set(2, [127, -127, 0]);
		color.set(2, [0, 128, 255, 255]);

		clear.run();
		program.run();

		return queue.then(() => {
			return expectPixelmatch(
				context,
				"spec/normalized-attribs/coords-and-color-2"
			);
		});
	});
});
