import Glii from "../../src/index.mjs";

describe("One triangle, one Float32 SingleAttribute", function () {
	it("renders red over clear green", function () {
		const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
		//
		const glii = new Glii(context);

		var vertexShaderSource = `
void main() {
	gl_Position = vec4(aCoords, 1.0);
}`;
		var fragmentShaderSource = `precision mediump float;
void main() {
  gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
}`;

		const coords = new glii.SingleAttribute({
			glslType: "vec3",
			type: Float32Array,
			size: 3,
			growFactor: false,
		});

		let indices = new glii.SequentialIndices({ size: 3 });

		// Using 64/127 (~=0,503937) instead of 0.5 in order for the
		// expectations to be the same as with the int8 test.
		let n = 64 / 127;
		coords.set(0, [-n, -n, -n]);
		coords.set(1, [-n, +n, +n]);
		coords.set(2, [+n, 0, 0]);

		let clear = new glii.WebGL1Clear({ color: [0.0, 1.0, 0.0, 1.0] });
		let program = new glii.WebGL1Program({
			vertexShaderSource: vertexShaderSource,
			varyings: {},
			fragmentShaderSource: fragmentShaderSource,
			indexBuffer: indices,
			attributes: { aCoords: coords },
		});

		clear.run();
		program.run();

		return expectPixelmatch(context, "spec/normalized-attribs/red-over-green", 1);
	});

	it("renders a triangle in a corner", function () {
		const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
		//
		const glii = new Glii(context);

		var vertexShaderSource = `
void main() {
	gl_Position = vec4(aCoords, 1.0);
}`;
		var fragmentShaderSource = `precision mediump float;
void main() {
  gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
}`;

		const coords = new glii.SingleAttribute({
			glslType: "vec3",
			type: Float32Array,
			size: 3,
			growFactor: false,
		});

		let indices = new glii.SequentialIndices({ size: 3 });

		// Using 112/127 (~=0,881889) instead of 0.9 in order for the expectations
		// to be the same as the int8 test.
		let n = 112 / 127;
		coords.set(0, [-1, -1, 0]);
		coords.set(1, [-1, -n, 0]);
		coords.set(2, [-n, -1, 0]);

		let clear = new glii.WebGL1Clear({ color: [0.0, 0.0, 0.0, 0.0] });
		let program = new glii.WebGL1Program({
			vertexShaderSource: vertexShaderSource,
			varyings: [],
			fragmentShaderSource: fragmentShaderSource,
			indexBuffer: indices,
			attributes: { aCoords: coords },
		});

		clear.run();
		program.run();

		return expectPixelmatch(context, "spec/normalized-attribs/one-corner");
	});
});

describe("One triangle, two Float32 `SingleAttribute`s", function () {
	it("One triangle with color per vertex", function () {
		const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
		const glii = new Glii(context);

		var vertexShaderSource = `
varying vec4 vColor;

void main() {
	vColor = aColor;
	gl_Position = vec4(aCoords, 1.0);
}`;
		var fragmentShaderSource = `precision mediump float;
varying vec4 vColor;

void main() {
  gl_FragColor = vColor;
}`;

		const coords = new glii.SingleAttribute({
			glslType: "vec3",
			size: 3,
			growFactor: false,
		});
		const color = new glii.SingleAttribute({
			glslType: "vec4",
			size: 3,
			growFactor: false,
		});

		let indices = new glii.SequentialIndices({ size: 3 });

		coords.set(0, [-100 / 127, -100 / 127, -64 / 127]);
		// color.set(0, [1.0, 0.0, 0.0, 1.0]);

		coords.set(1, [-90 / 127, +80 / 127, +0.5]);
		// color.set(1, [0.0, 1.0, 0.0, 1.0]);

		coords.set(2, [+100 / 127, -20 / 127, 0.0]);
		// color.set(2, [0.0, 0.0, 1.0, 1.0]);

		color.multiSet(
			0,
			[
				[1.0, 0.0, 0.0, 1.0],
				[0.0, 1.0, 0.0, 1.0],
				[0.0, 0.0, 1.0, 1.0],
			].flat()
		);

		let clear = new glii.WebGL1Clear({ color: [0.0, 0.0, 0.0, 0.0] });
		let program = new glii.WebGL1Program({
			vertexShaderSource: vertexShaderSource,
			varyings: [],
			fragmentShaderSource: fragmentShaderSource,
			indexBuffer: indices,
			attributes: { aCoords: coords, aColor: color },
		});

		clear.run();
		program.run();

		const queue = expectPixelmatch(
			context,
			"spec/normalized-attribs/coords-and-color",
			5
		);

		coords.set(0, [-1, -1, -0.5]);
		color.set(0, [0.5, 1.0, 0.0, 1.0]);

		coords.set(1, [-1, +1, +0.5]);
		color.set(1, [1.0, 0.0, 0.5, 1.0]);

		coords.set(2, [1.0, -1.0, 0.0]);
		color.set(2, [0.0, 0.5, 1.0, 1.0]);

		clear.run();
		program.run();

		return queue.then(() => {
			return expectPixelmatch(
				context,
				"spec/normalized-attribs/coords-and-color-2"
			);
		});
	});
});
