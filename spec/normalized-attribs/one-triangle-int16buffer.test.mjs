import Glii from "../../src/index.mjs";

describe("One triangle, one normalized Int16 SingleAttribute", function () {
	it("renders red over clear green", function () {
		const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
		//
		const glii = new Glii(context);

		var vertexShaderSource = `
void main() {
	gl_Position = vec4(aCoords, 1.0);
}`;
		var fragmentShaderSource = `precision mediump float;
void main() {
  gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
}`;

		const coords = new glii.SingleAttribute({
			glslType: "vec3",
			type: Int16Array,
			normalized: true,
			size: 3,
			growFactor: false,
		});

		let indices = new glii.SequentialIndices({ size: 3 });

		// Using 16640 (65*256) instead of 16384 (10^14 = 64*256) in order for the
		// expectations to be the same as with the int8 test.
		let n = 65 * 256;
		coords.set(0, [-n, -n, -n]);
		coords.set(1, [-n, +n, +n]);
		coords.set(2, [+n, 0, 0]);

		let clear = new glii.WebGL1Clear({ color: [0.0, 1.0, 0.0, 1.0] });
		let program = new glii.WebGL1Program({
			vertexShaderSource: vertexShaderSource,
			varyings: {},
			fragmentShaderSource: fragmentShaderSource,
			indexBuffer: indices,
			attributes: { aCoords: coords },
		});

		clear.run();
		program.run();

		return expectPixelmatch(context, "spec/normalized-attribs/red-over-green");
	});

	it("renders a triangle in a corner", function () {
		const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
		//
		const glii = new Glii(context);

		var vertexShaderSource = `
void main() {
	gl_Position = vec4(aCoords, 1.0);
}`;
		var fragmentShaderSource = `precision mediump float;
void main() {
  gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
}`;

		const coords = new glii.SingleAttribute({
			glslType: "vec3",
			type: Int16Array,
			normalized: true,
			size: 3,
			growFactor: false,
		});

		let indices = new glii.SequentialIndices({ size: 3 });

		let n = 128 * 256 - 1;
		let m = 112 * 256;
		coords.set(0, [-n, -n, 0]);
		coords.set(1, [-n, -m, 0]);
		coords.set(2, [-m, -n, 0]);

		let clear = new glii.WebGL1Clear({ color: [0.0, 0.0, 0.0, 0.0] });
		let program = new glii.WebGL1Program({
			vertexShaderSource: vertexShaderSource,
			varyings: [],
			fragmentShaderSource: fragmentShaderSource,
			indexBuffer: indices,
			attributes: { aCoords: coords },
		});

		clear.run();
		program.run();

		return expectPixelmatch(context, "spec/normalized-attribs/one-corner");
	});
});

describe("One triangle, two Int16+Uint16 `SingleAttribute`s", function () {
	it("One triangle with color per vertex", function () {
		const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
		const glii = new Glii(context);

		var vertexShaderSource = `
varying vec4 vColor;

void main() {
	vColor = aColor;
	gl_Position = vec4(aCoords, 1.0);
}`;
		var fragmentShaderSource = `precision mediump float;
varying vec4 vColor;

void main() {
  gl_FragColor = vColor;
}`;

		const coords = new glii.SingleAttribute({
			glslType: "vec3",
			type: Int16Array,
			normalized: true,
			size: 3,
			growFactor: false,
		});
		const color = new glii.SingleAttribute({
			glslType: "vec4",
			type: Uint16Array,
			normalized: true,
			size: 3,
			growFactor: false,
		});

		let indices = new glii.SequentialIndices({ size: 3 });

		coords.set(0, [-25700, -25700, -16384]);
		color.set(0, [65535, 0.0, 0.0, 65535]);

		coords.set(1, [-23130, +20320, +16384]);
		color.set(1, [0.0, 65535, 0.0, 65535]);

		coords.set(2, [+25600, -5120, 0.0]);
		color.set(2, [0.0, 0.0, 65535, 65535]);

		let clear = new glii.WebGL1Clear({ color: [0.0, 0.0, 0.0, 0.0] });
		let program = new glii.WebGL1Program({
			vertexShaderSource: vertexShaderSource,
			varyings: [],
			fragmentShaderSource: fragmentShaderSource,
			indexBuffer: indices,
			attributes: { aCoords: coords, aColor: color },
		});

		clear.run();
		program.run();

		const queue = expectPixelmatch(
			context,
			"spec/normalized-attribs/coords-and-color-int16"
		);

		coords.set(0, [-32767, -32767, -16384]);
		color.set(0, [32768, 65535, 0.0, 65535]);

		coords.set(1, [-32767, +32767, +16384]);
		color.set(1, [65535, 0.0, 32768, 65535]);

		coords.set(2, [32767, -32767, 0.0]);
		color.set(2, [0.0, 32768, 65535, 65535]);

		clear.run();
		program.run();

		return queue.then(() => {
			return expectPixelmatch(
				context,
				"spec/normalized-attribs/coords-and-color-2"
			);
		});
	});
});
