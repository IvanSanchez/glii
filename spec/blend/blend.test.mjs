import Glii from "../../src/index.mjs";

// See also: https://limnu.com/webgl-blending-youre-probably-wrong/

describe("Blend modes", function () {
	const context = contextFactory(256, 256, {
		premultipliedAlpha: true,
		preserveDrawingBuffer: true,
	});
	const glii = new Glii(context, {
		premultipliedAlpha: true,
		preserveDrawingBuffer: true,
	});

	const coordsColour = new glii.InterleavedAttributes(
		{
			size: 6,
			growFactor: false,
		},
		[
			{
				glslType: "vec3",
				type: Float32Array,
			},
			{
				glslType: "vec4",
				type: Uint8Array,
				normalized: true,
			},
		]
	);

	coordsColour.multiSet(0, [
		[
			[-0.8, -0.6, -0.2],
			[255, 0, 0, 192],
		],
		[
			[+0.8, -0.6, -0.2],
			[255, 0, 0, 192],
		],
		[
			[0, +0.8, -0.2],
			[255, 0, 0, 192],
		],
		[
			[-0.8, +0.6, +0.2],
			[0, 255, 0, 128],
		],
		[
			[+0.8, +0.6, +0.2],
			[0, 255, 0, 128],
		],
		[
			[0, -0.8, +0.2],
			[0, 255, 0, 128],
		],
	]);

	const clear = new glii.WebGL1Clear({ color: [0.0, 0.0, 0.0, 0.0] });
	const program = new glii.WebGL1Program({
		vertexShaderSource: `
void main() {
vColor = aColor;
gl_Position = vec4(aCoords, 1.0);
}`,
		varyings: { vColor: "vec4" },
		fragmentShaderSource: `
void main() {
gl_FragColor = vColor;
}`,
		indexBuffer: new glii.SequentialIndices({ size: 6 }),
		attributes: {
			aCoords: coordsColour.getBindableAttribute(0),
			aColor: coordsColour.getBindableAttribute(1),
		},
	});

	const pixelReadback = new Uint8Array(4);

	function expectPixels(red, green, center, none = [0, 0, 0, 0]) {
		// Read a pixel position covered only by red(ish) triangle
		glii.gl.readPixels(80, 80, 1, 1, glii.RGBA, glii.UNSIGNED_BYTE, pixelReadback);
		// 		console.log("red:", pixelReadback);

		// ...and compare with the expectation
		expect(pixelReadback).toEqual(Uint8Array.from(red));

		// Ditto with a pixel covered only by the green triangle...
		glii.gl.readPixels(80, 176, 1, 1, glii.RGBA, glii.UNSIGNED_BYTE, pixelReadback);
		expect(pixelReadback).toEqual(Uint8Array.from(green));

		// ...a pixel covered by both triangles...
		glii.gl.readPixels(128, 128, 1, 1, glii.RGBA, glii.UNSIGNED_BYTE, pixelReadback);
		expect(pixelReadback).toEqual(Uint8Array.from(center));

		// ...and finally a pixel covered by none of the triangles...
		glii.gl.readPixels(20, 128, 1, 1, glii.RGBA, glii.UNSIGNED_BYTE, pixelReadback);
		expect(pixelReadback).toEqual(Uint8Array.from(none));
	}

	function logPixels() {
		console.log("Reading pixels back from WebGL default framebuffer:");
		glii.gl.readPixels(80, 80, 1, 1, glii.RGBA, glii.UNSIGNED_BYTE, pixelReadback);
		console.log("red:", pixelReadback);

		glii.gl.readPixels(80, 176, 1, 1, glii.RGBA, glii.UNSIGNED_BYTE, pixelReadback);
		console.log("green:", pixelReadback);

		glii.gl.readPixels(128, 128, 1, 1, glii.RGBA, glii.UNSIGNED_BYTE, pixelReadback);
		console.log("center:", pixelReadback);

		glii.gl.readPixels(20, 128, 1, 1, glii.RGBA, glii.UNSIGNED_BYTE, pixelReadback);
		console.log("out:", pixelReadback);
	}

	it("blend off", function () {
		program.blend = false;

		clear.run();
		program.run();

		expectPixels([255, 0, 0, 192], [0, 255, 0, 128], [0, 255, 0, 128]);

		//logPixels();

		return expectPixelmatch(context, "spec/blend/blend-off", 20);
	});

	it("src over", function () {
		program.blend = {
			equationRGB: glii.FUNC_ADD,
			equationAlpha: glii.FUNC_ADD,
			srcRGB: glii.ONE,
			srcAlpha: glii.ONE,
			dstRGB: glii.ZERO,
			dstAlpha: glii.ZERO,
		};

		clear.run();
		program.run();

		return expectPixelmatch(context, "spec/blend/blend-src-over", 20);
	});

	it("dst over", function () {
		program.blend = {
			equationRGB: glii.FUNC_ADD,
			equationAlpha: glii.FUNC_ADD,
			srcRGB: glii.ZERO,
			srcAlpha: glii.ZERO,
			dstRGB: glii.ONE,
			dstAlpha: glii.ONE,
		};

		clear.run();
		program.run();

		return expectPixelmatch(context, "spec/blend/blend-dst-over", 5);
	});

	it("src-alpha + one-minus-src-alpha", function () {
		program.blend = {
			equationRGB: glii.FUNC_ADD,
			equationAlpha: glii.FUNC_ADD,
			srcRGB: glii.SRC_ALPHA,
			srcAlpha: glii.ONE_MINUS_SRC_ALPHA,
			dstRGB: glii.ONE,
			dstAlpha: glii.ONE_MINUS_SRC_ALPHA,
		};

		clear.run();
		program.run();

		expectPixels([192, 0, 0, 47], [0, 128, 0, 64], [192, 128, 0, 87]);

		return expectPixelmatch(
			context,
			"spec/blend/blend-src-alpha-and-one-minus-src-alpha",
			5
		);
	});

	it("one + one-minus-src-alpha", function () {
		program.blend = {
			equationRGB: glii.FUNC_ADD,
			equationAlpha: glii.FUNC_ADD,
			srcRGB: glii.ONE,
			srcAlpha: glii.ONE,
			dstRGB: glii.ONE_MINUS_SRC_ALPHA,
			dstAlpha: glii.ONE_MINUS_SRC_ALPHA,
		};

		clear.run();
		program.run();

		expectPixels([255, 0, 0, 192], [0, 255, 0, 128], [127, 255, 0, 224]);

		return expectPixelmatch(
			context,
			"spec/blend/blend-one-and-one-minus-src-alpha",
			8
		);
	});
});
