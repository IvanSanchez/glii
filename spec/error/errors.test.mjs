import Glii from "../../src/index.mjs";

describe("Error handling", function () {
	const context = contextFactory(256, 256, { preserveDrawingBuffer: true });
	const glii = new Glii(context);

	let indices = new glii.IndexBuffer({
		size: 4,
		growFactor: false,
		drawMode: glii.POINTS,
	});

	indices.set(0, [0, 1, 2, 3]);

	it(" throws on vertex shader line 1 on unknown symbol", function () {
		function go() {
			let program = new glii.WebGL1Program({
				vertexShaderSource: "unknownSymbol;",
				varyings: {},
				fragmentShaderSource: "",
				indexBuffer: indices,
				attributeBuffers: {},
			});
		}

		// 		return expect(go).toThrowError(Error)
		return expect(go).toThrowMatching(function (err) {
			return (
				err.message.includes("Could not compile vertex shader") &&
				err.message.includes("Around line 1: unknownSymbol;")
			);
		});
	});

	it(" throws on frag shader line 1 on unknown symbol", function () {
		function go() {
			let program = new glii.WebGL1Program({
				vertexShaderSource: "void main(void){}",
				varyings: {},
				fragmentShaderSource: "unknownSymbol;",
				indexBuffer: indices,
				attributeBuffers: {},
			});
		}

		// 		return expect(go).toThrowError(Error)
		return expect(go).toThrowMatching(function (err) {
			return (
				err.message.includes("Could not compile fragment shader") &&
				err.message.includes("Around line 1: unknownSymbol;")
			);
		});
	});

	it(" throws on frag shader line 3 on unknown symbol", function () {
		function go() {
			let program = new glii.WebGL1Program({
				vertexShaderSource: "void main(void){}",
				varyings: {},
				fragmentShaderSource:
					"void main(void) {\n\ngl_FragColor = unknownSymbol;\n}",
				indexBuffer: indices,
				attributeBuffers: {},
			});
		}

		// 		return expect(go).toThrowError(Error)
		return expect(go).toThrowMatching(function (err) {
			return (
				err.message.includes("Could not compile fragment shader") &&
				err.message.includes("Around line 3: gl_FragColor = unknownSymbol;")
			);
		});
	});

	it(" throws on invalid varying GLSL type", function () {
		function go() {
			let program = new glii.WebGL1Program({
				vertexShaderSource: "void main(void){}",
				varyings: { foobar: "double" },
				fragmentShaderSource: "void main(void){}",
				indexBuffer: indices,
				attributeBuffers: {},
			});
		}

		// 		return expect(go).toThrowError(Error)
		return expect(go).toThrowMatching(function (err) {
			return err.message.includes('but found "double"');
		});
	});

	it(" throws on invalid uniform GLSL type", function () {
		function go() {
			let program = new glii.WebGL1Program({
				vertexShaderSource: "void main(void){}",
				varyings: {},
				fragmentShaderSource: "void main(void){}",
				indexBuffer: indices,
				attributeBuffers: {},
				uniforms: { foobar: "double" },
			});
		}

		// 		return expect(go).toThrowError(Error)
		return expect(go).toThrowMatching(function (err) {
			console.log(err);
			return err.message.includes('but found "double"');
		});
	});

	it(" does *not* throw on GLSL types with precision qualifier", function () {
		function go() {
			let program = new glii.WebGL1Program({
				vertexShaderSource: "void main(void){}",
				varyings: {
					foobar: "mediump float",
					rgba: "lowp vec4",
					mat: "highp mat2",
				},
				fragmentShaderSource: "void main(void){}",
				indexBuffer: indices,
				attributeBuffers: {},
				uniforms: {
					ufoobar: "mediump float",
					urgba: "lowp vec4",
					umat: "highp mat2",
				},
			});
		}

		return expect(go).not.toThrowError(Error);
	});
});
