### Reminders about how to run the tests:

`puppeteer` needs to run twice, go get chromium nightly and firefox nightly:
PUPPETEER_PRODUCT=chrome npm install
PUPPETEER_PRODUCT=firefox npm install

Failing that:
PUPPETEER_PRODUCT=chrome nodejs node_modules/puppeteer/install.js
PUPPETEER_PRODUCT=firefox nodejs node_modules/puppeteer/install.js

Then:

npm run test

### Reminders about how to build the docs:

Leafdoc API and Leafdoc UML class diagram:

npm install
npm run docs

Turn diagram .dot files (graphviz, see https://graphviz.org/) into images:

apt-get install graphviz
dot -Tpng -O foo.dot

Turn primer markdown into HTML:

apt-get install cmark
cmark --unsafe primer.md > primer.html

### Changing the WebGL stack in desktop Chromium:

$ chromium --use-gl=desktop

$ chromium --use-gl=swiftshader

$ chromium --use-gl=egl

See https://peter.sh/experiments/chromium-command-line-switches/#use-gl
